/// <reference types="cypress" />

describe('Consultar CEP Válido cadastrado', () => {
    it('Realizar a consulta do CEP válido...', function () {
        cy.consultaCEPValido().should((response) => {
            expect(response.status).to.equal(200)
            cy.log(JSON.stringify(response.body))
        })
    })       
})