/// <reference types="cypress" />

describe('Consultar CEP inexistente na API', () => {
    it('Realizar a consulta do CEP inexistente...', function () {
        cy.consultaCEPInexistente().should((response) => {
            expect(response.status).to.equal(200)
            cy.log(JSON.stringify(response.body))
        })
    })       
})